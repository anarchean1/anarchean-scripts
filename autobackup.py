#!/usr/bin/env python3
import os
from threading import Thread
import shutil
import time
import sys
from glob import glob
import signal

class FileMonitor(Thread):
    def __init__(self, filename):
        Thread.__init__(self, group=None)
        self.filename = filename

        self.backup_dir = self.filename + ".backup"

        stat = os.stat(self.filename)
        self.last_modification = stat.st_mtime_ns
        self.monitoring = True
        self.modification_actions = []
        self.ending_actions = []

    def run(self):
        while (self.monitoring):
            stat = os.stat(self.filename)
            modification = stat.st_mtime_ns

            if (modification > self.last_modification):
                for action in self.modification_actions:
                    action(self.filename)
                    
                self.last_modification = modification

            time.sleep(1)

        for action in self.ending_actions:
            action(self.filename)

class BackupManager:
    def do_backup(self, filename):
        folder = self._check_for_backup(filename)

        files = glob(os.path.join(folder, "*"))

        if (len(files) >= 1):
            self._delete_older(folder, files, filename)

        self._create_backup(filename, folder)

    def _check_for_backup(self, filename):
        backup = filename + ".backup"

        exist = os.path.isdir(backup)

        if not exist:
            os.mkdir(backup)

        return backup

    def _delete_older(self, folder, files, filename):
        files = sorted(files)

        if (len(files) >= 3):
            #print(files)
            os.remove(files[len(files) - 1])

            files.remove(files[len(files) - 1])
            
        #print('after removal', files)

        new_files = []

        for x in range(0, len(files)):
            new_files.append(os.path.join(folder,
                                filename + '.bak' + str(x + 2)))
            
        zipped = list(zip(files, new_files))
        #print('zipped', list(zipped))

        for old, new in reversed(zipped):
            #print("moving '", old, "' to '", new, "'")
            shutil.move(old, new)

        return new_files

    def _create_backup(self, filename, folder):
        #print('Creating file')
        shutil.copyfile(filename, os.path.join(folder, filename + '.bak1'))


if __name__ == '__main__':
    manager = BackupManager()

    monitor = FileMonitor(sys.argv[1])
    monitor.modification_actions.append(manager.do_backup)
    monitor.start()
    
    def kill_monitor(signal, frame):
        global monitor
        print("Killing the monitor")
        monitor.monitoring = False
        monitor.join()
        sys.exit(0)
        
    signal.signal(signal.SIGINT, kill_monitor)
    signal.pause()
    
    
    